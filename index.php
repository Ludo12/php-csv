<?php
// connexion à la base de données.
$bd = new PDO("mysql:host=localhost;dbname=api_rest", "ludo", "ludo");

//nombre de fois max d'ajout de temps
$count = 3;
//minute à ajouter en seconde => min*second
$min = 1 * 10;
//temps de départ
$time = time();
//temps de fermeture
$timeClose = time() + ($count * $min);

//on génère les fichiers csv
while ($time <= $timeClose) {
    //on verifie le timer
    if (time() >= ($time + $min)) {
        //On fait la requete
        $request = $bd->query('SELECT * FROM produits');

        //On instancie le tableau
        $tabproduits = [];
        $tabproduits[] = ['ID', 'NOM', 'DESCRIPTION', 'PRIX', 'CATEGORIES_ID', 'CREATED_AT', 'UPDATED_AT'];
        $tabproduits[] = ['', '', ''];

        //On remplie le tableau avec les valeurs de la requete
        while ($resultat = $request->fetch(PDO::FETCH_ASSOC)) {
            $tabproduits[] = [
                $resultat['id'],
                $resultat['nom'],
                $resultat['description'],
                $resultat['prix'],
                $resultat['categories_id'],
                $resultat['created_at'],
                $resultat['updated_at']];
        }
        //On génère le nom du fichier
        $file = "produits." . date('Y-m-d_H:i:s', $time) . ".csv";

        //On ouvre le fichier en écriture
        $fichier_csv = fopen($file, 'w+');

        //On cré le fichier csv
        foreach ($tabproduits as $ligne) {
            $ligne = array_map("utf8_decode", $ligne);
            fputcsv($fichier_csv, $ligne, ";");
        }

        //On ferme le fichier
        fclose($fichier_csv);

        //Variable de path pour déplacer le fichier
        $dossierSource = __DIR__ . '/' . $file;
        $dossierDestination = "/home/ludo/Bureau/" . $file;

        //On déplace le fichier
        echo moveFile($dossierSource, $dossierDestination);

        //On incrémente $time
        $time += $min;
    }
}
//On ferme la connexion à la base de données.
$db = null;

/**
 * moveFile
 *
 * @param  string $dossierSource path source
 * @param  string $dossierDestination path destination
 * @return int
 */
function moveFile($dossierSource, $dossierDestination): int
{
//tout c'est bien passé le fichier a été copié et l'original supprimé
    $retour = 1;
    if (!file_exists($dossierSource)) {
        //le fichier existe pas
        $retour = -1;
    } else {
        if (!copy($dossierSource, $dossierDestination)) {
            //erreur avec la copie
            $retour = -2;
        } else {
            if (!unlink($dossierSource)) {
                //erreur suppression fichier source
                $retour = -3;
            }
        }
    }
    return ($retour);
}